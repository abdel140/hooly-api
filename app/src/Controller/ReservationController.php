<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Repository\FoodTruckRepository;
use App\Repository\PlacementRepository;
use App\Repository\ReservationRepository;
use App\Service\ReservationService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ReservationController extends AbstractController
{


    /**
     * Retrieve all reservations
     * @param ReservationRepository $reservationRepository
     * @return JsonResponse
     */
    #[Route('/api/reservations', name: 'get_reservations', methods: ["GET"])]
    public function getReservations(SerializerInterface $serializer, ReservationRepository $reservationRepository): JsonResponse
    {
        $reservationService = new ReservationService();
        return $this->json(
        [
            'reservations'=> $reservationService->getReservations($serializer, $reservationRepository)
        ],
        200);
    }

    #[Route('/api/reservation', name: 'add_new_reservation', methods: ["POST"])]
    public function addNewReservation(Request $request, FoodTruckRepository $foodTruckRepository, PlacementRepository $placementRepository, ReservationRepository $reservationRepository, EntityManagerInterface $em): JsonResponse
    {
        $reservationService = new ReservationService();
        try {
            return $reservationService->addNewReservation($request->getContent(), $foodTruckRepository, $placementRepository, $reservationRepository, $em);
        }
        catch (\Exception $e) {
            return $this->json([
                'code' => 404,
                'message' => $e->getMessage()
            ]);
        }
    }

}
