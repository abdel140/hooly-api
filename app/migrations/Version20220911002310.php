<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220911002310 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, placement_id INT NOT NULL, INDEX IDX_42C849552F966E9D (placement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C849552F966E9D FOREIGN KEY (placement_id) REFERENCES placement (id)');
        $this->addSql('ALTER TABLE placement_food_truck DROP FOREIGN KEY FK_71DAA5FE2F966E9D');
        $this->addSql('ALTER TABLE placement_food_truck DROP FOREIGN KEY FK_71DAA5FEEED85B8C');
        $this->addSql('DROP TABLE placement_food_truck');
        $this->addSql('ALTER TABLE food_truck ADD reservation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE food_truck ADD CONSTRAINT FK_3CAD36E0B83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id)');
        $this->addSql('CREATE INDEX IDX_3CAD36E0B83297E7 ON food_truck (reservation_id)');
        $this->addSql('ALTER TABLE placement ADD place VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE food_truck DROP FOREIGN KEY FK_3CAD36E0B83297E7');
        $this->addSql('CREATE TABLE placement_food_truck (placement_id INT NOT NULL, food_truck_id INT NOT NULL, INDEX IDX_71DAA5FE2F966E9D (placement_id), INDEX IDX_71DAA5FEEED85B8C (food_truck_id), PRIMARY KEY(placement_id, food_truck_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE placement_food_truck ADD CONSTRAINT FK_71DAA5FE2F966E9D FOREIGN KEY (placement_id) REFERENCES placement (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE placement_food_truck ADD CONSTRAINT FK_71DAA5FEEED85B8C FOREIGN KEY (food_truck_id) REFERENCES food_truck (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C849552F966E9D');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP INDEX IDX_3CAD36E0B83297E7 ON food_truck');
        $this->addSql('ALTER TABLE food_truck DROP reservation_id');
        $this->addSql('ALTER TABLE placement DROP place');
    }
}
