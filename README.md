# Hooly
Hooly is a company that rents foodtrucks. This is an API that allows foodtruckers to reserve their locations.

###### Contraintes:
1. Holly has 7 spots available for food trucks except Fridays when she only has 6.
2. Each foodtruck can only come once a week.
3. A foodtruck cannot book for the same day or for a past date.

###### Technos:
- Symfony 5.4
- PHP 8.1

###### Still needs:
- Cors management to allow certain subdomains to join the api
- Authentication or authorization via one of the protocol like JWT

###### Installation using Docker & Docker-compose:
- git clone into a folder on your local machine.
- ```docker-compose up -d```
- ```docker exec -it {docker_container_name or docker_container_id} bash```
  
  into docker container : #
- ```cd app```
- ```composer require symfony/runtime```
- ```php bin/console doctrine:schema:update --force```
- ```php bin/console doctrine:fixtures:load```
- ```symfony server:start -d```

Postman:
- get all reservations:
  http://127.0.0.1:9000/api/reservations

- example json to send to the api to book a reservation:
  http://127.0.0.1:9000/api/reservation
```  
{
  "placement_id" : 1,
  "foodtruck_id" : 10,
  "date" : "2023-09-26"
  }
```
