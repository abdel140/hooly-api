FROM php:8.1.10-fpm-alpine

RUN apk --no-cache update && apk --no-cache add bash git

#Installation de composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php composer-setup.php && php -r "unlink('composer-setup.php');" && mv composer.phar /usr/local/bin/composer

#Conneting to database
RUN docker-php-ext-install pdo_mysql

# Install intl extension
RUN apk add --no-cache icu-dev && \
    docker-php-ext-configure intl && \
    docker-php-ext-install intl

#Installation Symfony CLI
RUN  curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.alpine.sh' | bash && \
     apk add symfony-cli

ADD . /var/www/hooly

WORKDIR /var/www/hooly
