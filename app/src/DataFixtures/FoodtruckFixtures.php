<?php

namespace App\DataFixtures;

use App\Entity\FoodTruck;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FoodtruckFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for($i = 0; $i<10 ; $i++){
            $foodtruck = new FoodTruck();
            $foodtruck->setName("Foodtruck_$i");
            $foodtruck->setRegistrationPlate("XX-70$i.-FT");
            $manager->persist($foodtruck);
        }
        $manager->flush();
    }
}
