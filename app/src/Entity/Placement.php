<?php

namespace App\Entity;

use App\Repository\PlacementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PlacementRepository::class)]
class Placement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups("reservation")]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups("reservation")]
    private ?string $place = null;

    #[ORM\Column(type: Types::ARRAY)]
    #[Groups("reservation")]
    private array $days_available = [];

    #[ORM\OneToMany(mappedBy: 'placement', targetEntity: Reservation::class)]
    private Collection $reservations;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getDaysAvailable(): array
    {
        return $this->days_available;
    }

    public function setDaysAvailable(array $days_available): self
    {
        $this->days_available = $days_available;

        return $this;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations->add($reservation);
            $reservation->setPlacement($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getPlacement() === $this) {
                $reservation->setPlacement(null);
            }
        }

        return $this;
    }
}
