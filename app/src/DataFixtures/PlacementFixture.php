<?php

namespace App\DataFixtures;

use App\Entity\Placement;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PlacementFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $daysAvailable = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
         for($i = 1; $i<8 ; $i++){
             if($i == 7 ) {
                 array_splice($daysAvailable, 5, 1);
             }
             $placement = new Placement();
             $placement->setPlace("place_$i");
             $placement->setDaysAvailable($daysAvailable);

             $manager->persist($placement);
        }
        $manager->flush();
    }
}
