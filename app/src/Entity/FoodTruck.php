<?php

namespace App\Entity;

use App\Repository\FoodTruckRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: FoodTruckRepository::class)]
class FoodTruck
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups("reservation")]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups("reservation")]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups("reservation")]
    private ?string $registration_plate = null;

    #[ORM\OneToMany(mappedBy: 'foodtruck', targetEntity: Reservation::class)]
    private Collection $reservations;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRegistrationPlate(): ?string
    {
        return $this->registration_plate;
    }

    public function setRegistrationPlate(string $registration_plate): self
    {
        $this->registration_plate = $registration_plate;

        return $this;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations->add($reservation);
            $reservation->setFoodtruck($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getFoodtruck() === $this) {
                $reservation->setFoodtruck(null);
            }
        }

        return $this;
    }

}
