<?php


namespace App\Service;


use App\Entity\Placement;
use App\Entity\Reservation;
use App\Repository\FoodTruckRepository;
use App\Repository\PlacementRepository;
use App\Repository\ReservationRepository;
use Cassandra\Date;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;


class ReservationService
{

    public function getReservations(SerializerInterface $serializer, ReservationRepository $reservationRepository): string
    {
        return $serializer->serialize($reservationRepository->findAll(), 'json', ['groups' => ["reservation"]]);
    }

    /**
     * @throws \Exception
     */
    public function addNewReservation(String $json, FoodTruckRepository $foodTruckRepository, PlacementRepository $placementRepository, ReservationRepository $reservationRepository, EntityManagerInterface $em): JsonResponse
    {
        $reservationObject = json_decode($json, true);
        if($reservationObject == null ){
            throw  new \Exception("json format is not correct!");
        }
        if(!isset($reservationObject["date"]) ||  !isset($reservationObject["placement_id"]) || !isset($reservationObject["foodtruck_id"]) ){
            throw  new \Exception("one of the parameters is missing!");
        }
        if(!$this->isFormatDateCorrect($reservationObject["date"])){
            throw  new \Exception("Format date is not correct it must be as (yyyy-mm-dd)");
        }
        if(!is_int($reservationObject["foodtruck_id"])){
            throw new \Exception("foodtruck_id must be of type int");
        }
        if(!is_int($reservationObject["placement_id"])){
            throw new \Exception("placement_id must be of type int");
        }
        if(!$this->isFoodTruckExists($reservationObject["foodtruck_id"], $foodTruckRepository)){
            throw  new \Exception("Foodtruck does not exists ! ");
        }
        if(new DateTime($reservationObject["date"]) <= new DateTime("now")){
            throw new \Exception("you cannot book placement on the same day or before that!");
        }
        if($this->isAlreadyBookedSameWeek($reservationObject["placement_id"],$reservationObject["foodtruck_id"],$reservationObject["date"], $reservationRepository, $placementRepository)){
            throw new \Exception("you cannot book this placement in the same week!");
        }
        if($this->isPlacementAvailable($reservationObject["placement_id"],$reservationObject["date"], $reservationRepository, $placementRepository)){
            $reservation = new Reservation();
            $reservation->setFoodtruck($foodTruckRepository->find($reservationObject["foodtruck_id"]));
            $reservation->setPlacement($placementRepository->find($reservationObject["placement_id"]));
            $reservation->setDate(new \DateTime($reservationObject["date"]));
            $em->persist($reservation);
            $em->flush();
            return new JsonResponse([
                'code' => 201,
                'message' => "your reservation has been booked successfully !"
            ]);
        }else{
            return new JsonResponse([
                'code' => 404,
                'message' => "placement is not available !"
            ]);
        }
    }

    /**
     * @throws \Exception
     */
    public function isPlacementExist(int $placemenId, PlacementRepository $placementRepository): bool
    {
        $placement = $placementRepository->find($placemenId);
        if(!$placement){
            return false;
        }else{
            return true;
        }
    }

    /**
     * @throws \Exception
     */
    public function isFoodTruckExists(int $foodtruckId, FoodTruckRepository $foodTruckRepository): bool
    {
        $foodtruck = $foodTruckRepository->find($foodtruckId);
        if(!$foodtruck){
            return false;
        }else{
            return true;
        }
    }

    /**
     * @throws \Exception
     */
    public function isPlacementAvailable(int $placementId, String $date, ReservationRepository $reservationRepository, PlacementRepository $placementRepository):  bool
    {
        if(!$this->isPlacementExist($placementId, $placementRepository)){
            throw new \Exception("Placement does not exist!",404);
        }
        try {
            $date = new \DateTime($date);
        }catch (\Exception $e){
            throw new \Exception("Date is not available!",404);
        }
        try{
            $placement = $placementRepository->find($placementId);
            if(!in_array($date->format("D"),$placement->getDaysAvailable())){
                throw new \Exception("This placement is not available friday!",404);
            }
        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
        $reservations = $reservationRepository->findBy(["placement"=>$placementId]);
        foreach($reservations as $reservation){
            if(($date == $reservation->getDate())){
                return false;
            }
        }
        return true;
    }

    public function isFormatDateCorrect($date): bool|array
    {
        /*
         * TODO Validator
         */
        return (preg_grep("/^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/",[$date]));
    }

    public function isAlreadyBookedSameWeek(int $placementId,int $foodtruckId, String $date, ReservationRepository $reservationRepository, PlacementRepository $placementRepository): bool
    {
        $reservations = $reservationRepository->findBy(["placement"=>$placementId, "foodtruck"=>$foodtruckId]);
        foreach($reservations as $reservation){
            if((date('W' ,strtotime($date))) === $reservation->getDate()->format("W")){
                return true;
            }
        }
        return false;
    }
}