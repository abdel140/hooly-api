<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220911005652 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE food_truck DROP FOREIGN KEY FK_3CAD36E0B83297E7');
        $this->addSql('DROP INDEX IDX_3CAD36E0B83297E7 ON food_truck');
        $this->addSql('ALTER TABLE food_truck DROP reservation_id');
        $this->addSql('ALTER TABLE reservation ADD foodtruck_id INT NOT NULL');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955FD42418B FOREIGN KEY (foodtruck_id) REFERENCES food_truck (id)');
        $this->addSql('CREATE INDEX IDX_42C84955FD42418B ON reservation (foodtruck_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE food_truck ADD reservation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE food_truck ADD CONSTRAINT FK_3CAD36E0B83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_3CAD36E0B83297E7 ON food_truck (reservation_id)');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C84955FD42418B');
        $this->addSql('DROP INDEX IDX_42C84955FD42418B ON reservation');
        $this->addSql('ALTER TABLE reservation DROP foodtruck_id');
    }
}
